var dom = document.getElementById('chart-container');
var myChart = echarts.init(dom, null, {
  renderer: 'canvas',
  useDirtyRect: false
});
var app = {};
var option;
var arrayTsvData = [];


import myJsonData from './pgxCheckResults.json' assert {type: 'json'};

//Abspeichern der Patientendaten aus der Json in eine neue Variable.
var caseID = document.getElementById("caseID");
caseID.innerHTML += JSON.stringify(myJsonData.iDCasenumber).replaceAll('"', '');

var name = document.getElementById("name");
name.innerHTML += JSON.stringify(myJsonData.patientInfo.firstname)
  .replaceAll('"', '')
  .padEnd(6, ' ') +
  JSON.stringify(myJsonData.patientInfo.lastname).replaceAll('"', '');

var age = document.getElementById("age");
age.innerHTML += JSON.stringify(myJsonData.patientInfo.age).replaceAll('"', '');

var gender = document.getElementById("gender");
gender.innerHTML += JSON.stringify(myJsonData.patientInfo.gender).replaceAll('"', '');

var pregnant = document.getElementById("pregnant");
pregnant.innerHTML += JSON.stringify(myJsonData.patientInfo.possiblePregnancy).replaceAll('"', '');

var disease = document.getElementById("disease");
disease.innerHTML += JSON.stringify(myJsonData.patientInfo.preExistingDiseases[0]).replace('"', '');

var contact = document.getElementById("contact");
contact.innerHTML += JSON.stringify(myJsonData.patientInfo.contact.number).replaceAll('"', '');


//Loop durch Guideline, wenn Spezifischer, dann einsetzen der restlichen Elemente in Loop
for (var i = 0; i < myJsonData.guidelines.length; i++) {

  var guideline = document.getElementById("guideline");
  guideline.innerHTML +=
    '<li>' +
    '<label for ="specificGuideline' + [i] +
    '"; style="background: gray;" ><span>' + JSON.stringify(myJsonData.guidelines[i].name).replaceAll('"', '') + '</span>' +
    '</label>' +
    '<input type="checkbox" id="specificGuideline' + [i] + '">' +
    '<ul class="slide">' +
    '<li>' +
    "Gen: ".bold() + JSON.stringify(myJsonData.guidelines[i].gene.name).replaceAll('"', '') +
    '<br>' +
    "Diplotyp: ".bold()
    + JSON.stringify(myJsonData.guidelines[i].gene.diplotyp.haplotyp1).replaceAll('"', '') +"/ "
    + JSON.stringify(myJsonData.guidelines[i].gene.diplotyp.haplotyp2).replaceAll('"', '') +
    '<br>' +
    "Implikation: ".bold() + JSON.stringify(myJsonData.guidelines[i].implication).replaceAll('"', '') +
    '<br>' +
    "Enzymfunktion: ".bold()
    + JSON.stringify(myJsonData.guidelines[i].enzymFunction).replaceAll('"', '') +
    '<br>' +
    "Empfehlungen: ".bold()
    + JSON.stringify(myJsonData.guidelines[i].recommendation).split('. ', 1)[0].replaceAll('"', '') +
    '<br>' +
    "Aktivitätswert: ".bold()
    + JSON.stringify(myJsonData.guidelines[i].gene.activityScore).replaceAll('"', '') +
    '<br>' +
    "URL: ".bold()
    + '<a href="url" style="color: blue">' + JSON.stringify(myJsonData.guidelines[i].URL).replaceAll('"', '')

    + '</a>'
    + '</li>' +

    '</ul>' +
    '</li>';

}


var nodeValue;
var linkValue;
var nodes = [];
var links = [];
var drugPathway = [];

// Einlesen der tsv-Datei
arrayTsvData = await d3.tsv("./Metoprolol.tsv")

//Anpassen der eingelesenen tsv Datei (Stoffwechselweg)
for (let x = 0; x < arrayTsvData.length; x++) {
  drugPathway.push(new Array(arrayTsvData[x].From,
                             arrayTsvData[x].To,
                             arrayTsvData[x].Controller.split(', ')).flat());
}

var dict = [];
var zwischenSpeicher = [];
var itemIndex = 0


// Erstellung eines Dictionarys um doppelte Gene öfter im Graphen darstellen zu können.

for (let i = 0; i < drugPathway.length; i++) {
  for (let j = 0; j < drugPathway[i].length; j++) {
    var drugName = drugPathway[i][j]
    if(drugName != '') {
       giveKeyId(drugName, j)
    }
   
  }
  dict.push(zwischenSpeicher.flat())
  zwischenSpeicher = [];
}

//Überprüfe ob Element bereits im zwischenspeicher ist, wenn nicht füge hinzu
function giveKeyId(item, arrayIndex) {
  if (!(zwischenSpeicher.map(a => a.key).includes(item)) && arrayIndex < 2 ) {
     zwischenSpeicher.push({
      key: item,
      value: itemIndex
    })
    itemIndex += 1;
  } else if (arrayIndex > 1) {
    // Gene mit verschiedenen IDs erstellen
    zwischenSpeicher.push({
      key: item,
      value: itemIndex
    })
    itemIndex += 1;
  }
}

console.log(dict)

// Erstellung der Knoten und Kanten des Graphen
for (let i = 0; i < dict.length; i++) {
  for (let j = 0; j < dict[i].length; j++) {
  
  
    if (j < 2 && nodes.some(e => e.name === dict[i][j].key)) {
      const currentIndex = nodes.findIndex(x => x.name === dict[i][j].key);
      console.log("found it! " + dict[i][j].key + currentIndex)
      dict[i][j].value = currentIndex
    }
    
    //Startposition
    if (j == 0 && i == 0) {
      nodeValue = {

        id: dict[i][j].value,
        name: dict[i][j].key,
        x: 100,
        y: 150 + dict.length,
        symbol: "path://M63.92,64.71a13.57,13.57,0,0,1,19-2.46h0a13.56,13.56,0,0,1,2.46,19C76,93.41,66.62,105.46,57.23,117.64h0a13.47,13.47,0,0,1-18.88,2.45h0a13.5,13.5,0,0,1-2.45-18.88h0c9.39-12.2,18.62-24.3,28-36.5ZM4.44,85.1H40.16l3.76-4.89c4.47-5.84,8.93-11.65,14.92-19.43v0a19.73,19.73,0,0,1,5.4-4.82V42.57c.1-8.13-4.08-17.13-8.48-23l-1.4-1.88h2.39a5,5,0,0,0,5-5V5a5.05,5.05,0,0,0-5-5H8.06A5.05,5.05,0,0,0,3,5v7.64a5,5,0,0,0,5,5H9.84c-.51.7-1,1.38-1.53,2.06C3.24,26.56-.51,35.34.06,44.28v55a7.91,7.91,0,0,0,2.11,5.49c1.36,1.32,3.34,2,6.06,1.92H26.86a19.45,19.45,0,0,1,.5-2.48,18.28,18.28,0,0,1,.6-1.86l-18.61,0a4.94,4.94,0,0,1-3.58-1.19,4.7,4.7,0,0,1-1.24-2.79c0-.22-.09-13.26-.09-13.26ZM29.29,50.32H35a1.94,1.94,0,0,1,1.94,1.94v6.52h6.52a1.94,1.94,0,0,1,1.94,1.93v5.7a1.94,1.94,0,0,1-1.94,1.94H36.92v6.52A1.94,1.94,0,0,1,35,76.8H29.29a1.94,1.94,0,0,1-1.94-1.93V68.35H20.83a1.94,1.94,0,0,1-1.93-1.94v-5.7a1.94,1.94,0,0,1,1.93-1.93h6.52V52.26a1.94,1.94,0,0,1,1.94-1.94ZM59.51,42H4.79c0-2.07-.13.09.09-1.84A43.67,43.67,0,0,1,12.55,21.3c.81-1.08,1.64-2.21,2.5-3.42H49.16c.76,1.06,1.58,2.15,2.39,3.24,3.69,4.9,7.2,13,7.85,19.11.07.6.11,1.79.11,1.79Zm7.29,58.22L50.55,87.72,38.79,103h0a10.28,10.28,0,0,0,1.87,14.38h0A10.29,10.29,0,0,0,55,115.5h0L66.8,100.24Z",
        
        itemStyle: {
          color: 'purple'
        }

      }
      nodes.push(nodeValue);


    }
    //Endposition
    if (j == 1 && dict[i][j].key !== '') {
      nodeValue = {

        id: dict[i][j].value,
        name: dict[i][j].key,
        x: 28 * dict.length * (i+2),
        y: 150 + dict.length,
        
        symbol: "path://M2770 12689 c-147 -16 -327 -48 -420 -74 -30 -9 -80 -23 -110 -3 -30 -9 -69 -22 -87 -30 -17 -8 -36 -14 -42 -14 -11 0 -39 -12 -149 -61 -35 -16 -69 -29 -75 -29 -7 0 -17 -4 -22 -9 -6 -5 -44 -26 -85 -48 -41 -22 -91 -49 -110 -60 -345 -196 -713 -556 -933 -910 -77 -125 -184 -338 -220 -439 -86 -239 -122 -381 -158 -619 -25 -169 -27 -554 -4 -708 45 -295 89 -452 199 -719 99 -238 249 -481 431 -695 33 -39 447 -458 920 -931 638 -638 872 -866 905 -882 90 -43 134 -54 215 -55 84 0 140 12 215 48 65 31 3328 3292 3360 3357 38 79 51 123 57 195 7 84 -18 184 -64 259 -24 39 -1425 1454 -1725 1742 -212 204 -530 410 -798 518 -36 15 -72 30 -80 35 -9 5 -31 12 -50 16 -19 4 -41 11 -50 16 -8 4 -42 15 -75 24 -33 10 -82 23 -110 31 -94 27 -273 58 -430 74 -106 11 -398 11 -505 -1z m436 -900 c134 -12 332 -54 414 -87 14 -5 45 -18 70 -27 147 -58 324 -158 444 -252 93 -73 1426 -1403 1426 -1423 0 -19 -2510 -2530 -2529 -2530 -20 0 -1337 1324 -1412 1420 -279 358 -410 789 -378 1242 11 166 50 352 99 478 12 30 25 66 30 80 4 14 20 49 34 77 14 29 26 55 26 58 0 3 12 24 27 48 15 23 35 57 45 74 46 84 172 239 281 346 65 64 123 117 127 117 4 0 29 17 55 39 45 37 189 126 264 162 72 36 173 79 183 79 5 0 24 6 41 14 76 32 220 65 362 81 142 16 255 18 391 4z"
                +", path://M6084 8191 c-109 -27 -246 -136 -290 -231 -44 -96 -49 -115 -48 -215 0 -94 2 -105 36 -175 110 -232 380 -329 606 -218 217 107 310 374 206 593 -30 64 -130 172 -184 199 -100 50 -232 69 -326 47z"
                +", path://M4720 6835 c-219 -61 -363 -293 -321 -512 27 -137 128 -267 251 -324 58 -27 144 -49 191 -49 58 0 169 32 227 66 205 120 282 394 169 597 -19 34 -79 109 -107 134 -99 87 -277 125 -410 88z "
                +", path://M6075 6838 c-118 -38 -219 -116 -271 -208 -46 -80 -58 -129 -58 -235 0 -93 2 -105 33 -170 65 -135 166 -219 313 -259 74 -21 124 -20 206 3 91 25 145 56 208 119 244 240 130 658 -204 748 -59 16 -181 17 -227 2z"
                +", path://M3380 5491 c-70 -15 -154 -64 -216 -126 -73 -74 -112 -152 -126 -248 -6 -43 -8 -553 -5 -1331 4 -1345 2 -1309 52 -1521 8 -33 17 -76 20 -95 8 -41 13 -59 60 -205 20 -60 42 -124 50 -142 8 -17 15 -34 15 -37 0 -17 91 -204 144 -296 14 -25 29 -52 32 -60 13 -27 97 -153 158 -235 288 -387 706 -713 1136 -885 30 -12 64 -26 75 -31 39 -17 210 -71 280 -89 249 -62 399 -80 669 -80 324 0 513 28 816 122 88 27 248 86 288 107 9 5 60 30 112 56 340 168 629 403 904 736 114 137 264 387 351 584 107 243 167 457 209 750 14 96 16 269 16 1390 0 1241 -1 1282 -19 1335 -32 90 -52 122 -111 180 -62 61 -144 106 -221 120 -60 12 -4633 12 -4689 1z",
        
        symbolSize: 50,
        itemStyle: {
          //light Purple
          color: '#DA70D6'
        }
      }
      nodes.push(nodeValue);
      

      // Für Gene Guideline hinzufügen
    } else if (j > 1) {

      for (let k = 0; k < myJsonData.guidelines.length; k++) {
        var guidelineInfo = "";
        if (dict[i][j].key == (myJsonData.guidelines[k].gene.name).replaceAll('"', '')) {
          // Abfrage ob poor oder high metaboliser
          var arrowDosis;
          var dosis;
          var metaboliser = JSON.stringify(myJsonData.guidelines[k].enzymFunction).replaceAll('"', '');
          var metabolArrow;
          var borderSymbolColor = "";
          if (metaboliser == "Schwacher Metabolisierer") {
            arrowDosis = '<b><font size="+3">&#8599;</font></b>'
            dosis = '<span>&#9888;</span>'
            borderSymbolColor = '#06038D'
            metabolArrow = 'path://' + 'M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v5.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V4.5z'
          } else if (metaboliser == "Rapider Metabolisierer") {
            arrowDosis = '<b><font size="+3">&#8600;</font></b>'
            borderSymbolColor = '#FFC000'
            metabolArrow = 'path://' + 'M0 8a8 8 0 1 0 16 0A8 8 0 0 0 0 8zm5.904 2.803a.5.5 0 1 1-.707-.707L9.293 6H6.525a.5.5 0 1 1 0-1H10.5a.5.5 0 0 1 .5.5v3.975a.5.5 0 0 1-1 0V6.707l-4.096 4.096z'
          } else if (metaboliser == "Intermediärer Metabolisierer") {
            arrowDosis = '<b><font size="+3">&#8599;</font></b>'
            borderSymbolColor = 'blue'
            metabolArrow = 'path://' + 'M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm5.904-2.803a.5.5 0 1 0-.707.707L9.293 10H6.525a.5.5 0 0 0 0 1H10.5a.5.5 0 0 0 .5-.5V6.525a.5.5 0 0 0-1 0v2.768L5.904 5.197z'
          }
           else {
            arrowDosis = '<b><font size="+3">&#8594;</font></b>'
            borderSymbolColor = '#32CD32'
            metabolArrow = 'path://' + 'M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z'
          }

          var colorOfSeverity;
          var severity = JSON.stringify(myJsonData.guidelines[k].severity).replaceAll('"', '');
          // Abfrage für severity
          if (severity == "hoch") {
            colorOfSeverity = '<li style="list-style: none;"><label  style="background: red; width: 3px; height: 3px; margin-right: 3px"></label>high</li>'
          } else if (severity == "medium") {
            colorOfSeverity = '<li style="list-style: none;"><label  style="background: orange; width: 3px; height: 3px; margin-right: 3px"></label>medium</li>'
          } else if (severity == "niedrig") {
            colorOfSeverity = '<li style="list-style: none;"><label  style="background: lime; width: 3px; height: 3px; margin-right: 3px"></label>low</li>'
          }

          // Erstellung des Guideline Popups
          guidelineInfo = '<u>' +
            JSON.stringify(myJsonData.guidelines[k].name).replaceAll('"', '').bold() +
            '</u>' +
            '<br>' +
            "Dosierungsempfehlung: ".bold() + JSON.stringify(myJsonData.guidelines[k].drugDosis).replaceAll('"', '') + arrowDosis +
            '<br>' +
            "Enzymfunktion: ".bold() + JSON.stringify(myJsonData.guidelines[k].enzymFunction).replaceAll('"', '') +
            '<br>' +
            "Aktivitätswert: ".bold() + JSON.stringify(myJsonData.guidelines[k].gene.activityScore).replaceAll('"', '') +
            '<br>' +
            "Schweregrad: ".bold() + colorOfSeverity;
            break;
        } else {
          guidelineInfo = "Enzym im Patienten nicht vorhanden",
          borderSymbolColor = 'gray'
        }
      };
      
      
      
      // Gene für den ersten Durchlauf 
      if(i == 0) {
        nodeValue = {
          id: dict[i][j].value,
          name: dict[i][j].key,
         
          x: 131 + dict[i].length * (i*41),
          y: 75 + dict[i].length + j*20,
          symbol: metabolArrow,
          
          itemStyle: {
            //light blue
            color: '#89CFF0',
            borderWidth: 4,
            borderColor: borderSymbolColor,
           
            
          },
          tooltip: {
            formatter:
              guidelineInfo  // Guideline Popups einfügen
          },
          
        }
        
        //Gene für den zweiten Durchlauf
      } else {
        nodeValue = {
          id: dict[i][j].value,
          name: dict[i][j].key,
         
          x: 200 + dict.length,
          y: 33 + dict[i].length* (i*40),

          //Wenn im zweiten Durchlauf mehr Gene sind:
          //x: 100 + dict[i].length * (i*40),
          //y:  75 + dict[i].length + j*20,
          symbol: metabolArrow,
          
          itemStyle: {
            //light blue
            color: '#89CFF0',
            borderWidth: 4,
            borderColor: borderSymbolColor,
            
          },
          tooltip: {
            formatter:
              guidelineInfo
          }
        }
      }
      nodes.push(nodeValue);
    }
  


    //Überspringe, wenn source und target gleich sind (source 0, target 0)
    //oder wenn Source und Target nur einen Wert auseinander liegen (source: 0 , target 1)
    if(dict[i][j].value == dict[i][0].value 
      || dict[i][0].value == dict[i][j].value - 1
      //||  i > 0 && dict[i][0].value == 1 && dict[i][j].value == dict[i-1].length+1
      )
      {
      continue
    }
    console.log(dict[i].length)

    if(i > 0) {
      dict[i][j].value -= 1
    }

    //Verbindung vom Startpunkt zum Gen
    linkValue = {
      source: dict[i][0].value,
      target: dict[i][j].value
    }
    if(linkValue.source != linkValue.target
      && !(i > 0 && linkValue.source == 1  && linkValue.target == dict[i-1].length)) {
      links.push(linkValue);
    }
    console.log(dict)
    console.log(dict[i].length)
    //Verbindung vom Gen zum Endpunkt
    linkValue = {
      source: dict[i][j].value,
      target: dict[i][1].value
    } 
    if(linkValue.source != linkValue.target
      && !(i > 0 && linkValue.source == 1  && linkValue.target == dict[i-1].length)) {
      links.push(linkValue);
    }
    
    }
    
    if(linkValue.source != linkValue.target
      && !(i > 0 && linkValue.source == 1  && linkValue.target == dict[i-1].length)) {
      links.push(linkValue);
    }
    }

   

  //Duplikate von Nodes löschen.
  const uniqueNodes = []
  nodes = nodes.filter(element => {
    const isDuplicate = uniqueNodes.includes(element.id);
    if (!isDuplicate) {
      uniqueNodes.push(element.id);
      return true;
    }
    return false;
  });


console.log(nodes)
console.log(links)


option = {
  tooltip: {
    outside: true,
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle',
    borderWidth: 0
  },

  animationDurationUpdate: 1500,
  animationEasingUpdate: 'quinticInOut',

  series: [{
    type: 'graph',
    layout: 'none',
    symbolSize: 50,
    roam: true,
    label: {
      show: true,
      position: 'bottom',
    },
    edgeSymbol: ['circle', 'arrow'],
    edgeSymbolSize: [4, 10],
    edgeLabel: {
      fontSize: 25
    },

    data: nodes,
    links: links,

    lineStyle: {
      opacity: 0.9,
      width: 2,
      curveness: 0
    }
  }]

  
};

if (option && typeof option === 'object') {
  myChart.setOption(option);
  document.getElementById('chart-container').addEventListener('click', e => {
    e.target.innerHTML = 'Show series';
  });

  window.addEventListener('resize', myChart.resize);
}
